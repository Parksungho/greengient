package com.example.skyandgo;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;
import android.content.*;
import android.database.*;
import android.database.sqlite.*;
import android.graphics.*;

public class LocalDatabaseC
{
	private Context context;
	private DatabaseHelper dHelper;
	private SQLiteDatabase db;
	private ContentValues row;
	
	public LocalDatabaseC(Context context)
	{
		this.context=context;
		dHelper=new DatabaseHelper(context);
		db=dHelper.getReadableDatabase();
	}
	public void getMapCircle(GoogleMap mmap)
	{
		dHelper=new DatabaseHelper(context);
		db=dHelper.getReadableDatabase();
		Cursor cur1,cur2;
		cur1=db.rawQuery("SELECT sector,longitude,latitude FROM local",null);
		cur2=db.rawQuery("SELECT sector,maxindex FROM air",null);
		
		while(cur1.moveToNext())
		{
			LatLng position = new LatLng(cur1.getDouble(1),cur1.getDouble(2)); 
			
			while(cur2.moveToNext())
			{
				
				if(cur2.getInt(1)<50)
				{
					mmap.addCircle(new CircleOptions().center(position).fillColor(Color.argb(20,0,0,256))
							.radius(3000).strokeColor(Color.BLUE));
					break;
				}
				else if(50<cur2.getInt(1)&&cur2.getInt(1)<100)
				{
					mmap.addCircle(new CircleOptions().center(position).fillColor(Color.argb(20,29,219,22))
							.radius(3000).strokeColor(Color.GREEN));
					break;
				}
				else if(100<cur2.getInt(1)&&cur2.getInt(1)<150)
				{
					mmap.addCircle(new CircleOptions().center(position).fillColor(Color.argb(20,255,255,72))
							.radius(3000).strokeColor(Color.YELLOW));
					break;
				}
				else if(150<cur2.getInt(1)&&cur2.getInt(1)<250)
				{
					mmap.addCircle(new CircleOptions().center(position).fillColor(Color.argb(20,255,187,0))
							.radius(3000).strokeColor(Color.rgb(255,187,0)));
					break;
				}
				else if(250<cur2.getInt(1)&&cur2.getInt(1)<350)
				{
					mmap.addCircle(new CircleOptions().center(position).fillColor(Color.argb(20,255,0,0))
							.radius(3000).strokeColor(Color.rgb(255,0,0)));
					break;
				}
				else if(cur2.getInt(1)==0)
				{
					
				}
				else
				{
					mmap.addCircle(new CircleOptions().center(position).fillColor(Color.argb(20,153,0,133))
							.radius(3000).strokeColor(Color.rgb(153,0,133)));
					break;
				}
			}
		}
	}
}