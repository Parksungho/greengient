package com.example.skyandgo;

import android.os.Bundle;
import android.app.*;
import android.content.*;
import android.view.animation.*;
import android.webkit.*;
import android.widget.*;

/**
 * 추천공원과 지역을 검색하기 위한 액티비티
 * @author 박성호
 *
 */
public class WebActivity extends Activity 
{
	private WebView webview;
	private TextView t1,t2;
	private ImageView img;
	private Animation ani;
	/**
	 * 액티비티 설정 메소드
	 * @param Bundle savedInstanceState
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web);
		
		webview=(WebView)findViewById(R.id.webView1);
		
		webview.loadUrl("http://parks.seoul.go.kr/mobile/");
		webview.getSettings().setJavaScriptEnabled(true);
		t1=(TextView)findViewById(R.id.webtextView1);
		t2=(TextView)findViewById(R.id.webtextView2);
		img=(ImageView)findViewById(R.id.webimageView1);
		ani=new TranslateAnimation(0,400,0,0);
		ani.setDuration(1000);
		ani.setFillAfter(true);
		ani.setRepeatCount(Animation.INFINITE);
		ani.setRepeatMode(Animation.REVERSE);
		img.startAnimation(ani);
		Intent intent=getIntent();
		String text1=intent.getStringExtra("Sector");
		String text2=intent.getStringExtra("Park");
		int num=intent.getIntExtra("Index",0);
		t1.setText("추천 지역구: "+text1+", 대기 오염 지수 "+num);
		t2.setText("추천 공원: "+text2);
	}
}