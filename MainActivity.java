package com.example.skyandgo;

import javax.xml.parsers.*;

import org.w3c.dom.*;
import android.os.*;
import android.app.*;
import android.content.*;
import android.database.sqlite.*;
import android.view.*;
import android.widget.*;
/**
 * 메인 화면 액티비티
 * @author 박성호
 *
 */
public class MainActivity extends Activity 
{
	private DatabaseHelper dHelper;
	private SQLiteDatabase db;
	private ContentValues row;
	private AirDatabaseC airDB;
	private ParkDatabaseC parkDB;
	/**
	 * 메인화면 액티비티 설정을 위한 메소드
	 * @param Bundle savedInstanceState
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		new XmlNetwork().execute((Void)null);
		
		Intent fIntent = new Intent(this, FaceActivity.class);
		startActivity(fIntent);  
		setContentView(R.layout.activity_main);
		
		dHelper=new DatabaseHelper(this);
		db=dHelper.getWritableDatabase();
		row=new ContentValues();
		airDB=new AirDatabaseC(this);
		parkDB=new ParkDatabaseC(this);
	}
	/**
	 * 버튼 클릭 처리를 위한 메소드
	 * @param View v
	 */
	public void mOnClick(View v)
	{
		switch (v.getId())
		{
		case R.id.mainbutton1:
			Intent mIntent = new Intent(this, MapActivity.class);
			startActivity(mIntent);  
			break;
		case R.id.mainbutton2:
			String parkName="";
			int fst=500;
			fst=airDB.getMinIndex();
			String fstSector=airDB.getMinSector();
			if(fst==500)
			{
				Toast.makeText(this,"데이터 전송 오류 !!다시 시도해 주세요!",Toast.LENGTH_SHORT).show();
				new XmlNetwork().execute((Void)null);
			}
			else
			{
				parkName=parkDB.getParkInSector(fstSector);
				Intent wIntent = new Intent(this, WebActivity.class);
				wIntent.putExtra("Sector", fstSector);
				wIntent.putExtra("Park", parkName);
				wIntent.putExtra("Index", fst);
				startActivity(wIntent);
			}
			
			break;
		case R.id.mainbutton3:
			Intent lIntent = new Intent(this, RoadActivity.class);
			startActivity(lIntent);  
			break;
		}
	}
	/**
	 * 뒤로가기 버튼 처리를 위한 메소드
	 */
	public void onBackPressed()
	{
		new AlertDialog.Builder(this).setTitle(" ")
		.setMessage("종료 하시겠습니까?")
		.setPositiveButton("YES", new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int which)
			{
				db.execSQL("DELETE FROM air;");
				finish();
			}
		})
		.setNegativeButton("NO", null).setIcon(R.drawable.bye)
		.show();
	}
	/**
	 * URL로 부터 XML 정보를 읽어 데이터베이스에 저장하기 위한 클래스 
	 * @author 박성호
	 *
	 */
	class XmlNetwork extends AsyncTask<Void,Void,Void>
	{
		boolean initem=false;
		private String ItemName=" ";
		@Override
		/**
		 * 배경 쓰레드를 이용하는 메소드
		 * @param Void
		 */
		protected Void doInBackground(Void... arg0) 
		{
			// TODO Auto-generated method stub
			try
			{
				DocumentBuilderFactory factory=DocumentBuilderFactory.newInstance();
				DocumentBuilder builder=factory.newDocumentBuilder();
				Document doc1=builder.parse("http://openapi.seoul.go.kr:8088/707368626263383932313431/xml/ListAirQualityByDistrictService/1/25/");
				
				Element order1=doc1.getDocumentElement();
				
				NodeList items2=order1.getElementsByTagName("MSRSTENAME");
				NodeList items3=order1.getElementsByTagName("MAXINDEX");
				NodeList items4=order1.getElementsByTagName("GRADE");
				

				for(int i=0;i<25;i++)
				{
					Node item2 =items2.item(i);
					Node item3 =items3.item(i);
					Node item4 =items4.item(i);
			
					Node text2=item2.getFirstChild();
					Node text3=item3.getFirstChild();
					Node text4=item4.getFirstChild();
					
					
					String ItemName2=text2.getNodeValue();
					String ItemName3=text3.getNodeValue();
					String ItemName4=text4.getNodeValue();
					
					row.put("sector",ItemName2);
					row.put("maxindex",ItemName3);
					row.put("grade",ItemName4);
					
					db.insert("air",null,row);
				}
			}
			catch(Exception e)
			{
				
			}
			return null;
		}
	}
}

