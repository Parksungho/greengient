package com.example.skyandgo;

import android.content.*;
import com.google.android.gms.maps.*;

public class GoogleMaps
{
	private GoogleMap mmap;
	private Context context;
	private ParkDatabaseC parkDB;
	private LocalDatabaseC localDB;
	
	public GoogleMaps(GoogleMap map,Context context)
	{
		mmap=map;
		this.context=context;
		parkDB=new ParkDatabaseC(context);
		localDB=new LocalDatabaseC(context);
	}
	public void makeParkMaker()
	{
		parkDB.getLocalPark(mmap);
	}
	
	public void makeCircle()
	{
		localDB.getMapCircle(mmap);
	}
	public void makeMyLocMarker()
	{
		
	}
}