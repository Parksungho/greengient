package com.example.skyandgo;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;
import android.content.*;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.*;

public class MapActivity extends FragmentActivity implements LocationListener,OnMarkerClickListener 
{
	private GoogleMap mmap;
	private LocationManager locationManager;
	private MapJobRegi mapControl;
	private GoogleMaps map;
	ParkDatabaseC parkDB;
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);      
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE); 
	}
	
	@Override
	public void onBackPressed() 
	{
		this.finish();              
	}
	
	@Override
	protected void onResume() 
	{
		super.onResume();
		setUpMapIfNeeded();   
	}
	
	@Override
	protected void onPause() 
	{
		super.onPause();
		locationManager.removeUpdates(this);     
	}
	private void setUpMapIfNeeded()
	{
		LatLng position = new LatLng(37.5666091,126.978371); 
		if (mmap == null) 
		{   
			mmap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));
			
			map=new GoogleMaps(mmap,this);
			mapControl=new MapJobRegi();
			mapControl.setCommand(new GoogleMapsParkMarkCommand(map));
			mapControl.workOnMap();
			mapControl.setCommand(new GoogleMapsMakeCirCommand(map));
			mapControl.workOnMap();
			
			if (mmap != null) 
			{
				setUpMap();
			}
		}
	}
	
	private void setUpMap()
	{
		mmap.setOnMarkerClickListener(this);
		mmap.setMyLocationEnabled(true);
		mmap.getMyLocation();             
	}
	
	@Override
	public void onProviderDisabled(String provider)
	{
		// TODO Auto-generated method stub   
	}
	
	@Override
	public void onProviderEnabled(String provider)
	{
		// TODO Auto-generated method stub
	}
	
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras)
	{
		
	}
	
	@Override
	public boolean onMarkerClick(Marker marker)  
	{
		marker.getPosition(); 
		marker.showInfoWindow();
		marker.setTitle("공원 이름");  
		parkDB=new ParkDatabaseC(this);
		marker.setSnippet(""+parkDB.getEqualId(marker.getId())); 
		
		return true;
	}
	
	public boolean onCreateOptionsMenu(Menu menu)
	{
		super.onCreateOptionsMenu(menu);
		SubMenu item=menu.addSubMenu("지역구");
		item.add(0,1,0,"도봉구");
		item.add(0,2,0,"노원구");
		item.add(0,3,0,"강북구");
		item.add(0,4,0,"중랑구");
		item.add(0,5,0,"성북구");
		item.add(0,6,0,"종로구");
		item.add(0,7,0,"은평구");
		item.add(0,8,0,"서대문구");
		item.add(0,9,0,"중구");
		item.add(0,10,0,"동대문구");
		item.add(0,11,0,"광진구");
		item.add(0,12,0,"성동구");
		item.add(0,13,0,"용산구");
		item.add(0,14,0,"마포구");
		item.add(0,15,0,"강동구");
		item.add(0,16,0,"송파구");
		item.add(0,17,0,"강남구");
		item.add(0,18,0,"서초구");
		item.add(0,19,0,"동작구");
		item.add(0,20,0,"관악구");
		item.add(0,21,0,"영등포구");
		item.add(0,22,0,"구로구");
		item.add(0,23,0,"금천구");
		item.add(0,24,0,"양천구");
		item.add(0,25,0,"강서구");
		return true;
	}
	public boolean onOptionsItemSelected(MenuItem item)
	{
		LatLng position;
		switch(item.getItemId())
		{
		case 1:
			position = new LatLng(37.668768,127.047163);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 2:
			position = new LatLng(37.6543543,127.0564716);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 3:
			position = new LatLng(37.63974,127.025488);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 4:
			position = new LatLng(37.6063242,127.0925842);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));		
			return true;
		case 5:
			position = new LatLng(37.5893982,127.0167494);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 6:
			position = new LatLng(37.573025,126.979638);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 7:
			position = new LatLng(37.60278,126.9291627);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 8:
			position = new LatLng(37.579225,126.9368);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));		
			return true;
		case 9:
			position = new LatLng(37.563842,126.9976);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));		
			return true;
		case 10:
			position = new LatLng(37.5744934,127.0397652);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 11:
			position = new LatLng(37.538617,127.082375);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 12:
			position = new LatLng(37.563475,127.036838);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 13:
			position = new LatLng(37.532527,126.990487);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 14:
			position = new LatLng(37.5663244,126.901491);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 15:
			position = new LatLng(37.530126,127.1237708);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 16:
			position = new LatLng(37.514592,127.105863);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));		
			return true;
		case 17:
			position = new LatLng(37.517408,127.047313);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 18:
			position = new LatLng(37.4837522,127.0067046);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 19:
			position = new LatLng(37.51245,126.9395);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 20:
			position = new LatLng(37.4781548,126.9514847);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 21:
			position = new LatLng(37.5264324,126.8960076);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 22:
			position = new LatLng(37.495468,126.8875436);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 23:
			position = new LatLng(37.4570783,126.8957011);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));		
			return true;
		case 24:
			position = new LatLng(37.51701,126.8666435);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		case 25:
			position = new LatLng(37.5509358,126.8496421);
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));	
			return true;
		}
		return false;
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}
}
