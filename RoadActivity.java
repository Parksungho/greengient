package com.example.skyandgo;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.*;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

public class RoadActivity extends FragmentActivity implements LocationListener,OnMarkerClickListener 
{
	private GoogleMap mmap;
	private LocationManager locationManager;
	private String provider;
	private ParkDatabaseC parkDB;
	String text2;
	PolylineOptions op = new PolylineOptions(); 
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);      
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);  
		Criteria criteria = new Criteria();    
		provider = locationManager.getBestProvider(criteria, true);   
		locationManager.requestLocationUpdates(provider, 1, 1,this); 
		Intent intent=getIntent();
		text2=intent.getStringExtra("Park");
		parkDB=new ParkDatabaseC(this);
	}
	
	@Override
	public void onBackPressed() 
	{
		this.finish();              
	}
	
	@Override
	protected void onResume() 
	{
		super.onResume();
		setUpMapIfNeeded();   
	}
	
	@Override
	protected void onPause() 
	{
		super.onPause();
		locationManager.removeUpdates(this);     
	}
	
	private void setUpMapIfNeeded()
	{
		Location location = locationManager.getLastKnownLocation(provider); 
		LatLng position = new LatLng(location.getLatitude(),location.getLongitude());  
		if (mmap == null) 
		{   
			mmap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
			mmap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 13));
			mmap.setTrafficEnabled(true);
			mmap.addMarker(new MarkerOptions().position(position)); 
			if (mmap != null) 
			{
				setUpMap();
				mmap.addMarker(new MarkerOptions().position(position));
			}
		}
		
		parkDB.getRePark(mmap,text2);
		
	}
	
	private void setUpMap()
	{
		mmap.setOnMarkerClickListener(this);
		mmap.setMyLocationEnabled(true);
		mmap.getMyLocation();             
	}

	boolean locationTag = true;
	
	@Override
	public void onLocationChanged(Location location)
	{
		if (locationTag) 
		{
			Log.d("myLog", "onLocationChanged: !!" + "onLocationChanged!!");
			locationTag = false;
		}
	}
	
	@Override
	public void onProviderDisabled(String provider)
	{
		// TODO Auto-generated method stub   
	}
	
	@Override
	public void onProviderEnabled(String provider)
	{
		// TODO Auto-generated method stub
	}
	
	@Override
	public void onStatusChanged(String provider, int status, Bundle extras)
	{
		
	}
	
	@Override
	public boolean onMarkerClick(Marker marker)  
	{
		LatLng park=marker.getPosition();
		LatLng park=marker.getPosition();
		Location parkLo=new Location("park");
		parkLo.setLatitude(park.latitude);
		parkLo.setLongitude(park.longitude);
		Location location = locationManager.getLastKnownLocation(provider); 
		LatLng position = new LatLng(location.getLatitude(),location.getLongitude());
		double dis=location.distanceTo(parkLo);
		String recPark=parkDB.parkName((float)park.latitude,(float)park.longitude);
		double time1=dis/4000;
		
		marker.showInfoWindow();
		marker.setTitle(recPark);
		marker.setSnippet(""+"거리:"+(float)dis+"미터"+" 도보"+(int)time1+"시간 자전거 이용 시 "+(int)dis/12000+"소요 시간 예상");
		
		return true;
	}
}
