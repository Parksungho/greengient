package com.example.skyandgo;

import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.*;

import android.content.*;
import android.database.*;
import android.database.sqlite.*;

public class ParkDatabaseC
{
	private Context contex;
	private DatabaseHelper dHelper;
	private SQLiteDatabase db;
	private ContentValues row;
	
	public ParkDatabaseC(Context context)
	{
		this.contex=context;
		dHelper=new DatabaseHelper(context);
		db=dHelper.getReadableDatabase();
	}
	public String getParkInSector(String sector)
	{
		Cursor cur;
		cur=db.rawQuery("SELECT parkname,sector FROM park",null);
		String parkName="";
		while(cur.moveToNext())
		{
			if(sector.equals(cur.getString(1)))
				parkName+=" "+cur.getString(0);
		}
		return parkName;
	}
	public String getEqualId(String markId)
	{
		Cursor cur;
		cur=db.rawQuery("SELECT _id,parkname FROM park",null);
		String parkName="";
		while(cur.moveToNext())
		{
			String id="m"+(cur.getInt(0)-1);
			if(id.compareTo(markId)==0)
				 parkName+=cur.getString(1);
		}
		return parkName;
	}
	public void getLocalPark(GoogleMap map)
	{
		Cursor cur;
		cur=db.rawQuery("SELECT longitude,latitude,sector FROM park",null);
		while(cur.moveToNext())
		{
			map.addMarker(new MarkerOptions().position(new LatLng(cur.getDouble(0),cur.getDouble(1))));
		}
		
	}
}