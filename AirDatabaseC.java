package com.example.skyandgo;

import android.content.*;
import android.database.*;
import android.database.sqlite.*;
/**
 * 데이터베이스 air 테이블을 조작 하는 클래스
 * @author 박성호
 */
public class AirDatabaseC
{
	private Context contex;
	private DatabaseHelper dHelper;
	private SQLiteDatabase db;
	private ContentValues row;
	/**
	 * 생성자
	 * @param Context context
	 */
	public AirDatabaseC(Context context)
	{
		this.contex=context;
		dHelper=new DatabaseHelper(context);
		db=dHelper.getReadableDatabase();
	}
	/**
	 * 가장 작은 환경지수를 리턴하는 메소드
	 * @return int
	 */
	public int getMinIndex()
	{
		Cursor cur;
		int val;
		
		cur=db.rawQuery("SELECT sector,maxindex FROM air",null);
		int fst=500;
		while(cur.moveToNext())
		{
			val=cur.getInt(1);
			if(fst>val && val!=0)
			{
				fst=val;
			}
		}
		return fst;
	}
	/**
	 * 가장 작은 환경지수 지역을 리턴하는 메소드
	 * @return String
	 */
	public String getMinSector()
	{
		Cursor cur;
		int val;
		String fstSector="";
		
		cur=db.rawQuery("SELECT sector,maxindex FROM air",null);
		int fst=500;
		while(cur.moveToNext())
		{
			val=cur.getInt(1);
			if(fst>val && val!=0)
			{
				fst=val;
				fstSector=cur.getString(0);
			}
		}
		return fstSector;
	}
}