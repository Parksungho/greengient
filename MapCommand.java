package com.example.skyandgo;

public abstract class MapCommand
{
	public abstract void execute();
}