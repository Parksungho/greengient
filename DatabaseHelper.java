package com.example.skyandgo;

import android.content.*;
import android.database.sqlite.*;
/**
 * 데이터 베이스 생성을 위한 Helper 클래스
 * @author 박성호
 *
 */
class DatabaseHelper extends SQLiteOpenHelper
{
	/**
	 * 생성자
	 * @param Context context
	 */
	public DatabaseHelper(Context context)
	{
		super(context,"Air.db",null,1);	
	}
	/**
	 * 데이터 베이스 생성 메소드
	 * @param SQLiteDatabase db
	 */
	@Override
	public void onCreate(SQLiteDatabase db) 
	{
		// TODO Auto-generated method stub
		db.execSQL("CREATE TABLE air (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"sector TEXT, maxindex TEXT, grade TEXT);");
		db.execSQL("CREATE TABLE park (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"parkname TEXT, sector TEXT, longitude DOUBLE, latitude DOUBLE);");
		db.execSQL("CREATE TABLE local (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
				"sector TEXT, longitude DOUBLE, latitude DOUBLE);");
		
		db.execSQL("INSERT INTO local VALUES(null,'종로구',37.573025,126.979638);");
		db.execSQL("INSERT INTO local VALUES(null,'중구',37.563842,126.9976);");
		db.execSQL("INSERT INTO local VALUES(null,'용산구',37.532527,126.990487);");
		db.execSQL("INSERT INTO local VALUES(null,'성동구',37.563475,127.036838);");
		db.execSQL("INSERT INTO local VALUES(null,'광진구',37.538617,127.082375);");
		db.execSQL("INSERT INTO local VALUES(null,'동대문구',37.5744934,127.0397652);");
		db.execSQL("INSERT INTO local VALUES(null,'중랑구',37.6063242,127.0925842);");
		db.execSQL("INSERT INTO local VALUES(null,'성북구',37.5893982,127.0167494);");
		db.execSQL("INSERT INTO local VALUES(null,'강북구',37.63974,127.025488);");
		db.execSQL("INSERT INTO local VALUES(null,'도봉구',37.668768,127.047163);");
		db.execSQL("INSERT INTO local VALUES(null,'노원구',37.6543543,127.0564716);");
		db.execSQL("INSERT INTO local VALUES(null,'은평구',37.60278,126.9291627);");
		db.execSQL("INSERT INTO local VALUES(null,'서대문구',37.579225,126.9368);");
		db.execSQL("INSERT INTO local VALUES(null,'마포구',37.5663244,126.901491);");
		db.execSQL("INSERT INTO local VALUES(null,'양천구',37.51701,126.8666435);");
		db.execSQL("INSERT INTO local VALUES(null,'강서구',37.5509358,126.8496421);");
		db.execSQL("INSERT INTO local VALUES(null,'구로구',37.495468,126.8875436);");
		db.execSQL("INSERT INTO local VALUES(null,'금천구',37.4570783,126.8957011);");
		db.execSQL("INSERT INTO local VALUES(null,'영등포구',37.5264324,126.8960076);");
		db.execSQL("INSERT INTO local VALUES(null,'동작구',37.51245,126.9395);");
		db.execSQL("INSERT INTO local VALUES(null,'관악구',37.4781548,126.9514847);");
		db.execSQL("INSERT INTO local VALUES(null,'서초구',37.4837522,127.0067046);");
		db.execSQL("INSERT INTO local VALUES(null,'강남구',37.517408,127.047313);");
		db.execSQL("INSERT INTO local VALUES(null,'송파구',37.514592,127.105863);");
		db.execSQL("INSERT INTO local VALUES(null,'강동구',37.530126,127.1237708);");
	
		
		db.execSQL("INSERT INTO park VALUES(null,'초안산공원','노원구',37.6395713,127.0502889);");
		db.execSQL("INSERT INTO park VALUES(null,'발바닥공원','도봉구',37.6597134,127.0319819);");
		db.execSQL("INSERT INTO park VALUES(null,'서울창포원공원','도봉구',37.6897215,127.0479333);");
		db.execSQL("INSERT INTO park VALUES(null,'쌍문근린공원','도봉구',37.6531459,127.0271314);");
		db.execSQL("INSERT INTO park VALUES(null,'월천근린공원','도봉구',37.6473889,127.0509895);");
		db.execSQL("INSERT INTO park VALUES(null,'북서울꿈의숲','강북구',37.6233355,127.0407344);");
		db.execSQL("INSERT INTO park VALUES(null,'솔밭근린공원','강북구',37.652331,127.0116241);");
		db.execSQL("INSERT INTO park VALUES(null,'오동공원','강북구',37.6097564,127.0425764);");
		db.execSQL("INSERT INTO park VALUES(null,'중랑캠핑숲','중랑구',37.6057296,127.1110873);");
		db.execSQL("INSERT INTO park VALUES(null,'봉화산근린공원','중랑구',37.6106404,127.0923625);");
		db.execSQL("INSERT INTO park VALUES(null,'용마공원','중랑구',37.5946528,127.1050153);");
		db.execSQL("INSERT INTO park VALUES(null,'세종로공원','종로구',37.5735,126.9758875);");
		db.execSQL("INSERT INTO park VALUES(null,'낙산공원','종로구',37.580477,127.007587);");
		db.execSQL("INSERT INTO park VALUES(null,'마로니에공원','종로구',37.5803917,127.0027875);");
		db.execSQL("INSERT INTO park VALUES(null,'사직공원','종로구',37.5758147,126.9675055);");
		db.execSQL("INSERT INTO park VALUES(null,'삼청공원','종로구',37.5895821,126.9842875);");
		db.execSQL("INSERT INTO park VALUES(null,'탑골공원','종로구',37.5710052,126.9882853);");
		db.execSQL("INSERT INTO park VALUES(null,'백련산근린공원','서대문구',37.5879662,126.928616);");
		db.execSQL("INSERT INTO park VALUES(null,'궁동공원','서대문구',37.5690506,126.9229851);");
		db.execSQL("INSERT INTO park VALUES(null,'독립공원','서대문구',37.5732333,126.9585125);");
		db.execSQL("INSERT INTO park VALUES(null,'개운공원','성북구',37.5966128,127.0280219);");
		db.execSQL("INSERT INTO park VALUES(null,'성북공원','성북구',37.5948509,127.0078625);");
		db.execSQL("INSERT INTO park VALUES(null,'진관근린공원','은평구',37.6454628,126.9352968);");
		db.execSQL("INSERT INTO park VALUES(null,'봉산공원','은평구',37.604071,126.9020153);");
		db.execSQL("INSERT INTO park VALUES(null,'불광근린공원','은평구',37.6188016,126.9298962);");
		db.execSQL("INSERT INTO park VALUES(null,'남산공원','중구',37.551051,126.992509);");
		db.execSQL("INSERT INTO park VALUES(null,'손기정공원','중구',37.5560189,126.9652702);");
		db.execSQL("INSERT INTO park VALUES(null,'훈련원공원','중구',37.5675245,127.0038188);");
		db.execSQL("INSERT INTO park VALUES(null,'간데메공원','동대문구',37.5732438,127.0488644);");
		db.execSQL("INSERT INTO park VALUES(null,'답십리공원','동대문구',37.5750896,127.0626512);");
		db.execSQL("INSERT INTO park VALUES(null,'배봉산근린공원','동대문구',37.5812324,127.0641623);");
		db.execSQL("INSERT INTO park VALUES(null,'용두공원','동대문구',37.5732791,127.0390042);");
		db.execSQL("INSERT INTO park VALUES(null,'상암근린공원','마포구',37.5767,126.8859375);");
		db.execSQL("INSERT INTO park VALUES(null,'와우공원','마포구',37.5517298,126.9289936);");
		db.execSQL("INSERT INTO park VALUES(null,'월드컵공원','마포구',37.5649889,126.8974443);");
		db.execSQL("INSERT INTO park VALUES(null,'용산가족공원','용산구',37.522226,126.983335);");
		db.execSQL("INSERT INTO park VALUES(null,'효창공원','용산구',37.545234,126.95993);");
		db.execSQL("INSERT INTO park VALUES(null,'달맞이공원','성동구',37.543564,127.0212054);");
		db.execSQL("INSERT INTO park VALUES(null,'서울숲','성동구',37.5448142,127.0394911);");
		db.execSQL("INSERT INTO park VALUES(null,'성수공원','성동구',37.5448144,127.0524023);");
		db.execSQL("INSERT INTO park VALUES(null,'어린이대공원','광진구',37.5498987,127.0809212);");
		db.execSQL("INSERT INTO park VALUES(null,'구암공원','강서구',37.5679874,126.8526745);");
		db.execSQL("INSERT INTO park VALUES(null,'궁산공원','강서구',37.5735645,126.838952);");
		db.execSQL("INSERT INTO park VALUES(null,'뀡고개공원','강서구',37.5801585,126.8157075);");
		db.execSQL("INSERT INTO park VALUES(null,'매화공원','강서구',37.5575833,126.86115);");
		db.execSQL("INSERT INTO park VALUES(null,'방화공원','강서구',37.5823809,126.8119805);");
		db.execSQL("INSERT INTO park VALUES(null,'우장공원','강서구',37.5541932,126.844932);");
		db.execSQL("INSERT INTO park VALUES(null,'갈산공원','양천구',37.5099608,126.8698864);");
		db.execSQL("INSERT INTO park VALUES(null,'서서울호수공원','양천구',37.5278392,126.8304789);");
		db.execSQL("INSERT INTO park VALUES(null,'용왕산근린공원','양천구',37.5429593,126.8783125);");
		db.execSQL("INSERT INTO park VALUES(null,'파리공원','양천구',37.5349739,126.8763932);");
		db.execSQL("INSERT INTO park VALUES(null,'선유도공원','영등포구',37.5434191,126.9001354);");
		db.execSQL("INSERT INTO park VALUES(null,'여의도공원','영등포구',37.526129,126.922377);");
		db.execSQL("INSERT INTO park VALUES(null,'영등포공원','영등포구',37.515423,126.910662);");
		db.execSQL("INSERT INTO park VALUES(null,'앙카라공원','영등포구',37.5177329,126.9302895);");
		db.execSQL("INSERT INTO park VALUES(null,'고척공원','구로구',37.5042361,126.8522164);");
		db.execSQL("INSERT INTO park VALUES(null,'온수공원','구로구',37.5002598,126.83695);");
		db.execSQL("INSERT INTO park VALUES(null,'금천폭포공원','금천구',37.4468663,126.9040127);");
		db.execSQL("INSERT INTO park VALUES(null,'금천체육공원','금천구',37.468164,126.908681);");
		db.execSQL("INSERT INTO park VALUES(null,'국립현충원','동작구',37.4999236,126.9723177);");
		db.execSQL("INSERT INTO park VALUES(null,'노량진공원','동작구',37.5085091,126.954125);");
		db.execSQL("INSERT INTO park VALUES(null,'보라매공원','동작구',37.492333,126.918289);");
		db.execSQL("INSERT INTO park VALUES(null,'사육신공원','동작구',37.5137049,126.948636);");
		db.execSQL("INSERT INTO park VALUES(null,'삼일공원','동작구',37.4860183,126.9743686);");
		db.execSQL("INSERT INTO park VALUES(null,'서초문화예술공원','서초구',37.467654,127.030517);");
		db.execSQL("INSERT INTO park VALUES(null,'시민의숲','서초구',37.469928,127.0364741);");
		db.execSQL("INSERT INTO park VALUES(null,'도산공원','강남구',37.5246752,127.0350296);");
		db.execSQL("INSERT INTO park VALUES(null,'봉은공원','강남구',37.5139026,127.0554961);");
		db.execSQL("INSERT INTO park VALUES(null,'대모산공원','강남구',37.4796489,127.0809639);");
		db.execSQL("INSERT INTO park VALUES(null,'문정근린공원','송파구',37.4861763,127.1275875);");
		db.execSQL("INSERT INTO park VALUES(null,'송파나루공원','송파구',37.5069328,127.0983002);");
		db.execSQL("INSERT INTO park VALUES(null,'아시아공원','송파구',37.5102779,127.0762885);");
		db.execSQL("INSERT INTO park VALUES(null,'오금공원','송파구',37.5038715,127.1323957);");
		db.execSQL("INSERT INTO park VALUES(null,'올림픽공원','송파구',37.520934,127.122959);");
		db.execSQL("INSERT INTO park VALUES(null,'천마공원','송파구',37.4992929,127.1565625);");
		db.execSQL("INSERT INTO park VALUES(null,'길동생태공원','강동구',37.5403935,127.1547791);");
		db.execSQL("INSERT INTO park VALUES(null,'명일공원','강동구',37.5535443,127.1615731);");
		db.execSQL("INSERT INTO park VALUES(null,'샛마을공원','강동구',37.5504807,127.1471618);");
		db.execSQL("INSERT INTO park VALUES(null,'천호공원','강동구',37.5444741,127.1263451);");
		db.execSQL("INSERT INTO park VALUES(null,'낙성대공원','관악구',37.4713728,126.9603009);");
	}
	/**
	 * 데이터 베이스 갱신 메소드
	 * @param SQLiteDatabase db
	 * @param int oldVersion
	 * @param int newVersion
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) 
	{
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS air");
		db.execSQL("DROP TABLE IF EXISTS park");
		db.execSQL("DROP TABLE IF EXISTS local");
		onCreate(db);
	}
}