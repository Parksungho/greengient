package com.example.skyandgo;

import android.app.*;
import android.os.*;

/**
 * 로고화면을 위한 액티비티
 * @author 박성호
 */
public class FaceActivity extends Activity 
{
	private CountDownTimer fTimer = null; 
	/**
	 * 액티비티 설정을 위한 메소드
	 * @param Bundle saveInstanceState
	 */
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		fTimer = new CountDownTimer(5000, 1000) {  
			public void onTick(long millisUntilFinished) 
			{
				setContentView(R.layout.activity_face); 
			}
			public void onFinish() 
			{
				finish(); 
			}
		}.start();
	}
}
